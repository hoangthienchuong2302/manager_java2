
package assignment;

import java.util.Scanner;

public class menu {
        public void menu(){
        Scanner nhap= new Scanner(System.in);
        XuLy xl= new XuLy();
        String answer;
        while(true){
            System.out.println("=====================MENU======================");
            System.out.println("1. Nhập danh sách nhân viên                  ||");
            System.out.println("2. Xuất danh sách nhân viên                  ||");
            System.out.println("3. Tìm thông tin nhân viên bằng mã nhân viên ||");
            System.out.println("4. Xóa thông tin nhân viên theo mã           ||");
            System.out.println("5. Cập nhật thông tin nhân viên theo mã      ||");
            System.out.println("6. Tìm thông tin nhân viên theo lương        ||");
            System.out.println("7. Sắp xếp thông tin nhân viên theo họ tên   ||");
            System.out.println("8. Sắp xếp thông tin nhân viên theo thu nhập ||");
            System.out.println("9. Xuất 5 nhân viên có thu nhập cao nhất     ||");
            System.out.println("10. Kết thúc.                                ||");
            System.out.println("===============================================");
            System.out.println("Chọn 1 menu");
            int so = nhap.nextInt();
            switch(so){
                case 1: System.out.println("Chức năng nhập:"); 
                        xl.nhap();
                        break;
                case 2: System.out.println("Chức năng xuất sinh viên"); 
                        xl.xuat();
                        break;
                case 3: System.out.println("Chức năng tìm thông tin nhân viên bằng mã nhân viên");        
                        xl.timMANV();
                        break;
                case 4: System.out.println("Chức năng xóa thông tin nhân viên theo mã");
                        xl.xoaNV();
                        break;
                case 5: System.out.println("Chức năng cập nhật thông tin nhân viên theo mã");
                        xl.capNhat();
                        break;
                case 6: System.out.println("Chức năng tìm thông tin nhân viên theo lương");
                        xl.timLNV();
                        break;
                case 7: System.out.println("Chức năng Sắp xếp thông tin nhân viên theo họ tên");
                        xl.sapXepHoTen();
                        break; 
                case 8: System.out.println("Chức năng Sắp xếp thông tin nhân viên theo thu nhập");
                        xl.sapXepThuNhap();
                        break; 
                case 9: System.out.println("Chức năng xuất 5 nhân viên có thu nhập cao nhất");
                        xl.thuNhapCaoNhat();
                        break;                         
                case 10: break;
                default:System.out.println("Chức năng bạn chon không tồn tại");
            }
            while(true){
            Scanner nhap2=new Scanner(System.in);
            System.out.print("Bạn muốn sử dụng tiếp(y/n)?");
            answer= nhap2.nextLine();
            if(answer.equalsIgnoreCase("y")|| answer.equalsIgnoreCase("n")) break;
            }
            if( answer.equalsIgnoreCase("n"))break ;
        }
    }
}
