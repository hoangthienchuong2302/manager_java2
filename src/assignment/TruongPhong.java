
package assignment;

public class TruongPhong extends NhanVien {
    int trachNhiem;

    public TruongPhong(int trachNhiem, String hoTen, String maNV, String chucVu, int luong) {
        super(hoTen, maNV, chucVu, luong);
        this.trachNhiem = trachNhiem;
    }
    
    
     @Override
    public int getThuNhap() {
        return this.luong+this.trachNhiem;
    }
    
    @Override
    public double  getThueThuNhap(){
        if(getThuNhap()>9000&& getThuNhap()<15000)
            return getThuNhap()*0.1;
        else if(getThuNhap()>15000)
            return getThuNhap()*0.12;
        else return 0;
    }
    @Override
    public double thuNhapSauThue() {
        return getThuNhap()-getThueThuNhap();
    }
    @Override
    public void xuat(){
        super.xuat();
        System.out.println("Lương trách nhiệm là:"+trachNhiem);
        System.out.println("Thu nhập là:"+getThuNhap());
        System.out.println("Thuế thu nhập:"+getThueThuNhap());
        System.out.println("Tổng thu nhập sau thuế là:"+thuNhapSauThue());
         System.out.println("====================================================");
    }

}