
package assignment;

 public abstract class NhanVien {
     String hoTen, maNV;
            String chucVu;
     int luong;


    public NhanVien(String hoTen, String maNV, String chucVu, int luong) {
        super();
        this.hoTen = hoTen;
        this.maNV = maNV;
        this.chucVu = chucVu;
        this.luong = luong;
    }
    public abstract  int getThuNhap();
    
    public abstract double  getThueThuNhap();
    
    public abstract double thuNhapSauThue(); 
        
    
    public void xuat(){
        System.out.println("====================================================");
        System.out.println("Họ và tên nhân viên:"+this.hoTen);
        System.out.println("Mã nhân viên:"+this.maNV);
        System.out.println("Chức vụ nhân viên:"+this.chucVu);
        System.out.println("Lương:"+this.luong);
        
    }
}
