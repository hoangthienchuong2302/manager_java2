
package assignment;

public class NhanVienTiepThi extends NhanVien{
     int doanhSo,hueHong,giaTriHopDong;

    public NhanVienTiepThi(int doanhSo, int hueHong, int giaTriHopDong, String hoTen, String maNV, String chucVu,int luong) {
        super(hoTen, maNV, chucVu, luong);
        this.doanhSo = doanhSo;
        this.hueHong = hueHong;
        this.giaTriHopDong = giaTriHopDong;
    }

    public int doanhThu(){
        return this.giaTriHopDong*this.doanhSo;
    }
        
    @Override
    public int getThuNhap() {
        return this.luong+((doanhThu()*this.hueHong)/100);
    }
    @Override
    public double  getThueThuNhap(){
        if(getThuNhap()>9000&& getThuNhap()<15000)
            return getThuNhap()*0.1;
        else if(getThuNhap()>15000)
            return getThuNhap()*0.12;
        else return 0;
    }
    @Override
    public double thuNhapSauThue() {
        return getThuNhap()-getThueThuNhap();
    }
     @Override
    public void xuat(){
        super.xuat();
        System.out.println("Doanh thu của mỗi hợp đồng là:"+giaTriHopDong);
        System.out.println("Doanh số là:"+doanhSo);
        System.out.println("Tổng doanh thu là:" +doanhThu());
        System.out.println("Tỉ lệ huê hồng là:"+hueHong);
        System.out.println("Thu nhập là:"+getThuNhap());
        System.out.println("Thuế thu nhập:"+getThueThuNhap());
        System.out.println("Tổng thu nhập sau thuế là:"+thuNhapSauThue());
         System.out.println("====================================================");
    }
}
