
package assignment;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class XuLy {
    ArrayList<NhanVien> dsnv= new ArrayList<NhanVien>();
    public Scanner nhap= new Scanner(System.in);
    public Scanner nhap2= new Scanner(System.in);
    public Scanner nhap3= new Scanner(System.in);
    public String hoTen,maNV,timMaNV,chucVu,answer;
    public int luong,hueHong,doanhSo,giaTriHopDong,trachNhiem,dem,n;

    public void khaiBao(){
        while(giaTriHopDong<1000){
        System.out.println("Doanh thu của một hợp đồng là( >1000):");
        giaTriHopDong=nhap.nextInt();
        if(giaTriHopDong<1000000) System.out.println("Bạn nhập sai");
        }
        while(luong<1000){
        System.out.println("Nhập lương(>1000):");
        luong= nhap.nextInt();
        if(luong<1000) System.out.println("Bạn nhập sai");
        }
    }
    public void nhapNVTT(){
        do{
            System.out.println("Nhập họ và tên nhân viên:");
            hoTen=nhap3.nextLine(); 
            if(hoTen==null || hoTen.equalsIgnoreCase(""))System.out.println("Bạn nhập tên không đúng");
        }while(hoTen==null || hoTen.equalsIgnoreCase(""));
        while(true){
            System.out.println("Nhập mã nhân viên(4 ký tự):");
            maNV=nhap3.nextLine();
            if(!maNV.matches("\\w{1,3}")) break;// bai 6
            else System.out.println("Bạn nhập sai mã của nhân viên!!!");
        } 
                
        do{
            System.out.println("Nhập doanh số:");
            doanhSo= nhap.nextInt();
            if(doanhSo<0) System.out.println("Bạn đã nhập sai");
        }while(doanhSo<0);

                
        do{
            System.out.println("Nhập tỷ lệ huê hồng(1-100)");
            hueHong= nhap.nextInt();
            if(hueHong<0 && hueHong>100) System.out.println("Bạn đã nhập sai:");
        }while(hueHong<0 && hueHong>100);
                
        NhanVienTiepThi NVTT=new NhanVienTiepThi(doanhSo, hueHong, giaTriHopDong, hoTen, maNV, "Nhân viên tiếp thị", luong);
        dsnv.add(NVTT);    
    }
    public void nhapTP(){
        do{
            System.out.println("Nhập họ và tên nhân viên");
            hoTen=nhap3.nextLine();
            if(hoTen==null || hoTen.equalsIgnoreCase(""))System.out.println("Bạn nhập tên không đúng");
        }while(hoTen==null || hoTen.equalsIgnoreCase(""));
                
        while(true){
            System.out.println("Nhập mã nhân viên(4 ký tự):");
            maNV=nhap3.nextLine();
            if(!maNV.matches("\\w{1,3}")) break;
            else System.out.println("Bạn nhập sai mã của nhân viên!!!");
        } 
                
                
        do{
            System.out.println("Nhập lương trách nhiệm(>1000)");
            trachNhiem= nhap.nextInt();
            if(trachNhiem<1000) System.out.println("Bạn nhập sai!!!");
        }while(trachNhiem<1000);
                
        TruongPhong TP= new TruongPhong(trachNhiem, hoTen, maNV, "Trưởng phòng", luong);
        dsnv.add(TP);
    }
    public void nhapNVHC(){
        do{
            System.out.println("Nhập họ và tên nhân viên");
            hoTen=nhap3.nextLine();
            if(hoTen==null || hoTen.equalsIgnoreCase(""))System.out.println("Bạn nhập tên không đúng");
        }while(hoTen==null || hoTen.equalsIgnoreCase(""));
                
                
        while(true){
            System.out.println("Nhập mã nhân viên(4 ký tự):");
             maNV=nhap3.nextLine();
            if(!maNV.matches("\\w{1,3}")) break;
            else System.out.println("Bạn nhập sai mã của nhân viên!!!");
        } 
                
                
        NhanVienHanhChanh HC = new NhanVienHanhChanh(hoTen, maNV, "Nhân viên hành chánh", luong);
        dsnv.add(HC);
    }
    public void nhap(){
        khaiBao();
        System.out.println("Nhập số nhân viên cần nhập:");
        n= nhap.nextInt();
        for(int i=0;i<n;i++){
            while(true){
            System.out.println("Nhập chức vụ của nhân viên(Tiếp thị-TT / Trưởng phòng-TP / Hành chính-HC):");
            chucVu= nhap2.nextLine();
            if(chucVu.equalsIgnoreCase("TT")||chucVu.equalsIgnoreCase("TP")||chucVu.equalsIgnoreCase("HC"))break;
            else System.out.println("Xin lỗi! Chức vụ không tồn tại");
            }
            if(chucVu.equalsIgnoreCase("TT")){
                nhapNVTT();
                for(int j=i-1;j==0;j++){
                    if(dsnv.get(i).maNV.equalsIgnoreCase(dsnv.get(j).maNV)){
                    do{
                        System.out.println("Bạn đã bị trùng mã nhân viên, mời bạn nhập lại:");
                        dsnv.get(i).maNV=nhap3.nextLine();
                    }while(dsnv.get(i).maNV.equalsIgnoreCase(dsnv.get(j).maNV));
                }
                }
            System.out.println("================================================");
            }

            else if(chucVu.equalsIgnoreCase("TP")){
                nhapTP();
                for(int j=i-1;j==0;j++){
                    if(dsnv.get(i).maNV.equalsIgnoreCase(dsnv.get(j).maNV)){
                    do{
                        System.out.println("Bạn đã bị trùng mã nhân viên, mời bạn nhập lại:");
                        dsnv.get(i).maNV=nhap3.nextLine();
                    }while(dsnv.get(i).maNV.equalsIgnoreCase(dsnv.get(j).maNV));
                    }
                }
            System.out.println("================================================");
            }
           else if(chucVu.equalsIgnoreCase("HC")){
               nhapNVHC();
                for(int j=i-1;j==0;j++){
                    if(dsnv.get(i).maNV.equalsIgnoreCase(dsnv.get(j).maNV)){
                    do{
                        System.out.println("Bạn đã bị trùng mã nhân viên, mời bạn nhập lại:");
                        dsnv.get(i).maNV=nhap3.nextLine();
                    }while(dsnv.get(i).maNV.equalsIgnoreCase(dsnv.get(j).maNV));
                    }
                }
            System.out.println("================================================");
            }    
        }
    }

    public void xuat(){
        for(int i=0;i<n;i++){
            dsnv.get(i).xuat();
        }
    }
    public void timMANV(){
        dem=0;
        System.out.println("Nhập mã nhân viên cần tìm:");
        timMaNV = nhap3.nextLine();
        for(NhanVien x: dsnv){
        if(x.maNV.equalsIgnoreCase(timMaNV)){ 
            do{
                while(true){
                    System.out.print("Bạn muốn tìm thông tin của nhân viên "+x.hoTen+"(y/n)?::\n");
                    answer=nhap3.nextLine();
                    if(answer.equalsIgnoreCase("y")|| answer.equalsIgnoreCase("n"))break;
                    }
                        if(answer.equalsIgnoreCase("n")){
                            System.out.println("Nhập mã nhân viên cần tìm:");
                            timMaNV = nhap3.nextLine();
                        }
                }while(answer.equalsIgnoreCase("n"));
            System.out.println("Nhân viên cần tìm:");
            x.xuat();
            dem++;
        }  
        }if(dem==0)System.out.println("Không tìm thấy nhân viên có mã này");
    }
    public void xoaNV(){
        dem=0;
        System.out.println("Nhập mã nhân viên cần xóa:");
        timMaNV = nhap3.nextLine();
        for(NhanVien x:dsnv){
            if(x.maNV.equalsIgnoreCase(timMaNV)){
                do{
                    while(true){
                    System.out.print("Bạn muốn xóat thông tin của nhân viên "+x.hoTen+"(y/n)?:\n");
                    answer=nhap3.nextLine();
                    if(answer.equalsIgnoreCase("y")|| answer.equalsIgnoreCase("n"))break;
                    }
                        if(answer.equalsIgnoreCase("n")){
                            System.out.println("Nhập mã nhân viên cần xóa:");
                            timMaNV = nhap3.nextLine();
                        }
                }while(answer.equalsIgnoreCase("n"));
                dsnv.remove(x);
                n-=1;
                dem++;
            }        
        }if(dem==0)System.out.println("Không tìm thấy nhân viên có mã này");
        xuat();
    }
    public void capNhat(){
        dem=0;
        System.out.println("Nhập mã nhân viên cần cập nhật:");
        timMaNV = nhap3.nextLine();
            for(int i=0;i<n;i++){
                if(dsnv.get(i).maNV.equalsIgnoreCase(timMaNV)){
                    do{
                        while(true){
                            System.out.print("Bạn muốn cập nhật thông tin của nhân viên "+dsnv.get(i).hoTen+"(y/n)?:\n");
                            answer=nhap3.nextLine();
                            if(answer.equalsIgnoreCase("y")|| answer.equalsIgnoreCase("n"))break;
                            }
                        if(answer.equalsIgnoreCase("n")){
                            System.out.println("Nhập mã nhân viên cần cập nhật:");
                            timMaNV = nhap3.nextLine();
                        }
                    }while(answer.equalsIgnoreCase("n"));
                        
                        while(true){    
                        System.out.println("Bạn muốn đổi chức vụ của nhân viên không(y/n)?");
                        answer=nhap3.nextLine();
                        if(answer.equalsIgnoreCase("y")|| answer.equalsIgnoreCase("n"))break;
                        }
                        if(answer.equalsIgnoreCase("y")){
                            System.out.println("Nhập chức vụ của nhân viên(Tiếp thị-TT / Trưởng phòng-TP / Hành chính-HC):");
                            dsnv.get(i).chucVu=nhap2.nextLine();
                            while(true){
                                if(dsnv.get(i).chucVu.equalsIgnoreCase("TT")||dsnv.get(i).chucVu.equalsIgnoreCase("TP")||dsnv.get(i).chucVu.equalsIgnoreCase("HC"))break;
                                else System.out.println("Xin lỗi! Chức vụ không tồn tại");
                            }
                                if(dsnv.get(i).chucVu.equalsIgnoreCase("TT")){
                                    nhapNVTT();
                                    for(int j=i-1;j==0;j++){
                                        if(dsnv.get(i).maNV.equalsIgnoreCase(dsnv.get(j).maNV)){
                                        do{
                                            System.out.println("Bạn đã bị trùng mã nhân viên, mời bạn nhập lại:");
                                            dsnv.get(i).maNV=nhap3.nextLine();
                                        }while(dsnv.get(i).maNV.equalsIgnoreCase(dsnv.get(j).maNV));
                                    }
                                    }
                                System.out.println("================================================");
                                }

                                else if(dsnv.get(i).chucVu.equalsIgnoreCase("TP")){
                                    nhapTP();
                                    for(int j=i-1;j==0;j++){
                                        if(dsnv.get(i).maNV.equalsIgnoreCase(dsnv.get(j).maNV)){
                                        do{
                                            System.out.println("Bạn đã bị trùng mã nhân viên, mời bạn nhập lại:");
                                            dsnv.get(i).maNV=nhap3.nextLine();
                                        }while(dsnv.get(i).maNV.equalsIgnoreCase(dsnv.get(j).maNV));
                                        }
                                    }
                                System.out.println("================================================");
                                }
                                
                               else if(dsnv.get(i).chucVu.equalsIgnoreCase("HC")){
                                   nhapNVHC();
                                    for(int j=i-1;j==0;j++){
                                        if(dsnv.get(i).maNV.equalsIgnoreCase(dsnv.get(j).maNV)){
                                        do{
                                            System.out.println("Bạn đã bị trùng mã nhân viên, mời bạn nhập lại:");
                                            dsnv.get(i).maNV=nhap3.nextLine();
                                        }while(dsnv.get(i).maNV.equalsIgnoreCase(dsnv.get(j).maNV));
                                        }
                                    }
                                System.out.println("================================================");
                                }
                        }
                        
                        
                        
                        if(dsnv.get(i).chucVu.equalsIgnoreCase("Nhân viên tiếp thị")||dsnv.get(i).chucVu.equalsIgnoreCase("TT")){
                            dsnv.remove(i);
                            nhapNVTT();
                               for(int j=i-1;j==0;j++){
                                        if(dsnv.get(i).maNV.equalsIgnoreCase(dsnv.get(j).maNV)){
                                        do{
                                            System.out.println("Bạn đã bị trùng mã nhân viên, mời bạn nhập lại:");
                                            dsnv.get(i).maNV=nhap3.nextLine();
                                        }while(dsnv.get(i).maNV.equalsIgnoreCase(dsnv.get(j).maNV));
                                        }
                               }
                        }
                        
                        
                        else if(dsnv.get(i).chucVu.equalsIgnoreCase("Trưởng phòng")) {
                            dsnv.remove(i);
                            nhapTP();
                               for(int j=i-1;j==0;j++){
                                        if(dsnv.get(i).maNV.equalsIgnoreCase(dsnv.get(j).maNV)){
                                        do{
                                            System.out.println("Bạn đã bị trùng mã nhân viên, mời bạn nhập lại:");
                                            dsnv.get(i).maNV=nhap3.nextLine();
                                        }while(dsnv.get(i).maNV.equalsIgnoreCase(dsnv.get(j).maNV));
                                        }
                               }
                        }
                        
                        
                        else if(dsnv.get(i).chucVu.equalsIgnoreCase("Nhân viên hành chánh")){
                            dsnv.remove(i);
                            nhapNVHC();
                               for(int j=i-1;j==0;j++){
                                        if(dsnv.get(i).maNV.equalsIgnoreCase(dsnv.get(j).maNV)){
                                        do{
                                            System.out.println("Bạn đã bị trùng mã nhân viên, mời bạn nhập lại:");
                                            dsnv.get(i).maNV=nhap3.nextLine();
                                        }while(dsnv.get(i).maNV.equalsIgnoreCase(dsnv.get(j).maNV));
                                        }
                               }
                        }
                        dem++;
                        break;
            }
        }if(dem==0) System.out.println("Không tìm thấy nhân viên có mã này");
        System.out.println("Danh sách sau khi cập nhật:");
        xuat();
    }
    public void timLNV(){
        dem=0;
        System.out.println("Nhập khoảng lương cần tìm:");
        System.out.print("Từ:");
        int minLuong=nhap.nextInt();
        System.out.print("Đến:");
        int maxLuong=nhap.nextInt();
        for(NhanVien x: dsnv){
            if(minLuong<x.thuNhapSauThue() && x.thuNhapSauThue()<maxLuong){ 
                System.out.println("Nhân viên cần tìm:");
                x.xuat();
                dem++;
            } 
        }if(dem==0) System.out.println("Không tìm thấy nhân viên có mã này");
    }
    public void sapXepHoTen(){
        dem=0;
        System.out.println("Sắp xếp danh sách theo họ tên:");
            Comparator<NhanVien> comp= new Comparator<NhanVien>() {
                @Override
                public int compare(NhanVien o1, NhanVien o2) {
                    return o2.hoTen.compareTo(o1.hoTen);
                }
            };
        Collections.sort(dsnv,comp);
        xuat();
    }
    public void sapXepThuNhap(){
        dem=0;
        System.out.println("Sắp xếp danh sách theo thu nhập sau thuế:");
        Comparator<NhanVien> comp= new Comparator<NhanVien>() {
            @Override
            public int compare(NhanVien o1, NhanVien o2) {
                return o2.thuNhapSauThue()<o1.thuNhapSauThue()?-1:1;
            }
        };
        Collections.sort(dsnv,comp);
        xuat();
    }
    public void thuNhapCaoNhat(){
        dem=0;
        System.out.println("5 nhân viên có thu nhập cao nhất:");
         Comparator<NhanVien> comp= new Comparator<NhanVien>() {
            @Override
            public int compare(NhanVien o1, NhanVien o2) {
                return o2.thuNhapSauThue()<o1.thuNhapSauThue()?-1:1;
            }
        };
        Collections.sort(dsnv,comp);
        for(int i=0;i<5;i++){
            dsnv.get(i).xuat();
        }
    }
}
